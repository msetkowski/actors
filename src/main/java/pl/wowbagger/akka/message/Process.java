package pl.wowbagger.akka.message;

import java.io.Serializable;

public class Process implements Serializable {

	private static final long serialVersionUID = -4918926064264051491L;
	private String data;
	
	public Process(String data) {
		this.data = data;
	}
	
	public String getData() {
		return data;
	}
}
