package pl.wowbagger.akka.message;

import java.io.Serializable;

public class InitAction implements Serializable {

	private static final long serialVersionUID = -6401264564838004478L;
	private int counter = 200000;

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}
	
}
