package pl.wowbagger.akka.message;

import java.io.Serializable;

import scala.concurrent.duration.Duration;

public class Done implements Serializable {
	
	private static final long serialVersionUID = -5205685923985704652L;
	private Duration duration;

	public Done(Duration duration) {
		this.duration = duration;
	}

	public Duration getDuration() {
		return duration;
	}
}
