package pl.wowbagger.akka.actor;

import static akka.actor.SupervisorStrategy.escalate;
import static akka.actor.SupervisorStrategy.restart;
import static akka.actor.SupervisorStrategy.resume;
import static akka.actor.SupervisorStrategy.stop;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import pl.wowbagger.akka.message.Done;
import pl.wowbagger.akka.message.InitAction;
import pl.wowbagger.akka.message.Process;
import pl.wowbagger.akka.message.Response;
import scala.concurrent.duration.Duration;
import akka.actor.ActorRef;
import akka.actor.OneForOneStrategy;
import akka.actor.Props;
import akka.actor.SupervisorStrategy;
import akka.actor.SupervisorStrategy.Directive;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.Function;
import akka.routing.RoundRobinRouter;

public class Supervisor extends UntypedActor {
	private final ActorRef listener;
	private final ActorRef workerDispatcher;
	private int nrOfResults = 0;
	private int nrOfMessages;
	private int nrOfWorkers;
	private final long start = System.currentTimeMillis();
	LoggingAdapter log = Logging.getLogger(getContext().system(), this);

	private static SupervisorStrategy strategy = new OneForOneStrategy(10, Duration.create("1 minute"),
			new Function<Throwable, Directive>() {
				@Override
				public Directive apply(Throwable t) {
					if (t instanceof ArithmeticException) {
						return resume();
					} else if (t instanceof NullPointerException) {
						return restart();
					} else if (t instanceof IllegalArgumentException) {
						return stop();
					} else {
						return escalate();
					}
				}
			});

	@Override
	public SupervisorStrategy supervisorStrategy() {
		return strategy;
	}

	public Supervisor(int numberOfWorkers, ActorRef listener) {
		this.nrOfWorkers = numberOfWorkers;
		this.listener = listener;
		workerDispatcher = this.getContext().actorOf(new Props(Processor.class).withRouter(new RoundRobinRouter(numberOfWorkers)),
				"workerRouter");

		// variant based on dispatcher - configuration inside application.conf
		// file
		// workerDispatcher = this.getContext().actorOf(new
		// Props(Processor.class).withDispatcher("basic-dispatcher"));
	}

	@Override
	public void onReceive(Object message) throws Exception {
		if (message instanceof InitAction) {
			int noMessages = ((InitAction) message).getCounter();
			nrOfMessages = noMessages;
			log.info("number of messages: " + noMessages);
			log.info("number of workers: " + nrOfWorkers);
			// sending worker messages to through dispatcher
			for (int i = 0; i < noMessages; i++) {
				workerDispatcher.tell(new Process(UUID.randomUUID().toString()), getSelf());
			}
		} else if (message instanceof Response) {
			nrOfResults += 1;
			if (nrOfResults == nrOfMessages) {
				// Send the result to the listener
				Duration duration = Duration.create(System.currentTimeMillis() - start, TimeUnit.MILLISECONDS);
				listener.tell(new Done(duration), getSelf());
				// Stops this actor and all its supervised children
				getContext().stop(getSelf());
			}
		} else {
			unhandled(message);
		}

	}

}
