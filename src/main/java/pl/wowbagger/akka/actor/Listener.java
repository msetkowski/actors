package pl.wowbagger.akka.actor;

import pl.wowbagger.akka.message.Done;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class Listener extends UntypedActor {
	LoggingAdapter log = Logging.getLogger(getContext().system(), this);

	@Override
	public void onReceive(Object message) throws Exception {
		if (message instanceof Done) {
			Done msg = (Done) message;
			log.info("Processing time: " + msg.getDuration().toString());
			this.getContext().system().shutdown();
		} else {
			unhandled(message);
		}

	}

}
