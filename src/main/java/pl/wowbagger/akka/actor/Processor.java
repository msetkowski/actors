package pl.wowbagger.akka.actor;

import pl.wowbagger.akka.exception.ProcessorException;
import pl.wowbagger.akka.message.Process;
import pl.wowbagger.akka.message.Response;
import scala.concurrent.duration.Duration;
import akka.actor.OneForOneStrategy;
import akka.actor.SupervisorStrategy;
import akka.actor.SupervisorStrategy.Directive;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.Function;
import static akka.actor.SupervisorStrategy.escalate;
import static akka.actor.SupervisorStrategy.restart;

public class Processor extends UntypedActor {

	LoggingAdapter log = Logging.getLogger(getContext().system(), this);

	private static SupervisorStrategy strategy = new OneForOneStrategy(3, Duration.create("5 seconds"),
			new Function<Throwable, Directive>() {
				@Override
				public Directive apply(Throwable t) {
					if (t instanceof ProcessorException) {
						return restart();
					} else {
						return escalate();
					}
				}
			});

	@Override
	public SupervisorStrategy supervisorStrategy() {
		return strategy;
	}

	@Override
	public void onReceive(Object msg) throws Exception {

		if (msg instanceof Process) {
			Process message = (Process) msg;
			// log.info(((Process) msg).getData());
			doMsg(message);
			getSender().tell(new Response(), getSelf());
		} else {
			unhandled(msg);
		}

	}

	private void doMsg(Process message) throws ProcessorException {
		// something, something dark side
	}

}
