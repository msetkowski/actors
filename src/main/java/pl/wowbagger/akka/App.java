package pl.wowbagger.akka;

import pl.wowbagger.akka.actor.Listener;
import pl.wowbagger.akka.actor.Supervisor;
import pl.wowbagger.akka.message.InitAction;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.actor.UntypedActorFactory;

public class App {

	@SuppressWarnings("serial")
	public static void main(String[] args) {

		ActorSystem system = ActorSystem.create("ProcessingSystem");

		//listener actor
		final ActorRef listener = system.actorOf(new Props(Listener.class), "listener");
		//Control actor for particular workers
		ActorRef supervisor = system.actorOf(new Props(
				new UntypedActorFactory() {
					public UntypedActor create() {
						return new Supervisor(16, listener);
					}
				}), "supervisor");
		//initial message to start work
		supervisor.tell(new InitAction(), system.deadLetters());
		
		
	}

}
